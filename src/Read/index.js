import React, { useContext } from "react";
import ReactHtmlParser from "react-html-parser";
import { useParams, Link } from "react-router-dom";
import { BlogContext } from "../BlogContext";
import { BreadCrumbsContainer } from "./Read.style";

export default function Read() {
  const blog = useContext(BlogContext);
  const { pcode } = useParams();
  const post = blog.searchPostByCode(pcode);

  return (
    <div>
      { post ? (
          <>
            <BreadCrumbsContainer>
              <Link to="/">Home</Link> <small>&nbsp;/&nbsp;</small> Read Post - {post.title}
            </BreadCrumbsContainer>
            <h2>{post.title}</h2>
            {ReactHtmlParser(post.description)}
          </>
        ) : "Loading..."
      }
    </div>
  );
}