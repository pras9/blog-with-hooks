import React, { useContext, useEffect } from "react";
import { createMemoryHistory } from "history";
import { render, screen } from "@testing-library/react";
import {
  Router,
  Switch,
  Route
} from "react-router-dom";
import { BlogContext, BlogProvider } from "../../BlogContext";
import Services from "../../BlogContext/Services";
import Read from "../index";

const blogDataMock = {
  "name": "Test Blog",
  "description": "A small test blog",
  "posts": [
    {
      "id": "1201",
      "pcode": "the-first-test-post",
      "title": "Test the first post title",
      "intro": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
      "description": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>"
    }
  ]
};

jest.mock("../../BlogContext/Services", () => {
  return ({
    fetchBlogData: (cb) => {
      cb(blogDataMock);
    }
  });
});

function ReadCont() {
  const blog = useContext(BlogContext);
  useEffect(() => {
    Services.fetchBlogData((data) => {
      blog.setPosts(data.posts);
    });
  });

  const history = createMemoryHistory()
  history.push('/p/the-first-test-post');
  return (
    <Router history={history}>
      <Switch>
        <Route path="/p/:pcode">
          <Read />
        </Route>
      </Switch>
    </Router>
  );
}

describe("<Read />", () => {

  test("renders the blog read page", () => {
    render(
      <BlogProvider>
        <ReadCont />
      </BlogProvider>
    );
    const elem = screen.getByText(/Read Post -/ig);
    expect(elem).toBeInTheDocument();
    const readElem = screen.getByText(/consectetur adipiscing elit, sed do eiusmod tempor incididunt/ig);
    expect(readElem).toBeInTheDocument();
  });

  test("renders the blog read page with no content", () => {
    const history = createMemoryHistory()
    history.push('/p/the-first-test-post');
    render(
      <BlogProvider>
        <Router history={history}>
          <Switch>
            <Route path="/p/:pcode">
              <Read />
            </Route>
          </Switch>
        </Router>
      </BlogProvider>
    );
    const readElem = screen.getByText(/Loading.../ig);
    expect(readElem).toBeInTheDocument();
  });

});