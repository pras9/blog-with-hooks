import styled from "@emotion/styled";

export const BreadCrumbsContainer = styled("div")`
  font-size: 0.8em;
  color: #888888;
`;