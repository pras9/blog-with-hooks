import styled from "@emotion/styled";

export const ListContainer = styled("div")`
  ul.blog-list {
    list-style: none;
    padding: 0;
    li {
      margin-bottom: 30px;
      .post-intro {
        font-size: 0.85em;
        margin-top: 4px;
      }
    }
  }
`;