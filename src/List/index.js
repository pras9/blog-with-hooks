import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { BlogContext } from "../BlogContext";
import {
  ListContainer
} from "./List.style"

export default function List() {
  const blog = useContext(BlogContext);

  return (
    <ListContainer>
      <h2>Blog Posts</h2>
      {blog.posts.length > 0 ? 
        (
          <ul className="blog-list">
            {blog.posts.map((post, index) => (
              <li key={post.pcode}>
                <Link to={`/p/${post.pcode}`}>{post.title}</Link>
                <div className="post-intro">{post.intro}</div>
              </li>
            ))}
          </ul>
        )
      : "No Posts!"
    }
    </ListContainer>
  );
};