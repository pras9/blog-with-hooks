import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BlogProvider } from "./BlogContext";

ReactDOM.render(
  <BlogProvider>
    <App />
  </BlogProvider>,
  document.getElementById("root")
);