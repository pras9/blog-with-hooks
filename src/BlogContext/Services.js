export const fetchBlogData = (cb) => {
  fetch("/data/blog-data.json")
    .then(response => response.json())
    .then(data => {
      cb(data);
    });
};
