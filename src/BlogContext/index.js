import React, { createContext, useState } from "react";

export const BlogContext = createContext();

// This context provider is passed to any component requiring the context
export const BlogProvider = ({ children }) => {
  const [name, setName] = useState("unknown");
  const [description, setDescription] = useState("Introduction");
  const [posts, setPosts] = useState([]);
  const searchPostByCode = pcode => posts.find((post, index) => post.pcode === pcode);

  return (
    <BlogContext.Provider value={{name, setName, description, setDescription, posts, setPosts, searchPostByCode}}>
      {children}
    </BlogContext.Provider>
  );
};