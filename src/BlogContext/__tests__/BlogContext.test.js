import React, { useContext, useEffect } from "react";
import { render, screen } from "@testing-library/react";
import { BlogProvider, BlogContext } from "../index";

describe('<BlogProvider />', () => {

  test("exposes set methods to its children", () => {

    function ChildElem() {
      const blog = useContext(BlogContext);
      useEffect(() => {
        blog.setName("New Test Blog");
        blog.setDescription("Test Blog Description");
      });
      
      return (
      <div>{blog.name} <small>{blog.description}</small></div>
      );
    }

    render(
      <BlogProvider><ChildElem /></BlogProvider>
    );
    const elem = screen.getByText(/New Test Blog/ig);
    expect(elem).toBeInTheDocument();
    const elemDesc = screen.getByText(/Test Blog Description/ig);
    expect(elemDesc).toBeInTheDocument();
  });

});