import styled from "@emotion/styled";

export const AppContainer = styled("div")`
  max-width: 940px;
  padding: 0 5px;
  margin: 0 auto;
  font-family: "Arial";
  a {
    text-decoration: none;
  }
`;

export const Header = styled("header")`
  padding: 20px 0;
`;

export const Main = styled("div")`
  margin: 10px 0 20px 0;
`;

export const Footer = styled("footer")`
  margin: 50px 0;
  padding-top: 5px;
  text-align: center;
  border-top: 2px solid #CCCCCC;
`;