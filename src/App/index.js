import React, { useContext, useEffect } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { BlogContext } from "../BlogContext";
import List from "../List";
import Read from "../Read";
import { 
  AppContainer,
  Header,
  Footer,
  Main
} from "./App.style";
import { fetchBlogData } from "../BlogContext/Services";

export default function App() {
  const blog = useContext(BlogContext);
  useEffect(() => {
    fetchBlogData(data => {
      blog.setName(data.name);
      blog.setDescription(data.description);
      blog.setPosts(data.posts);
    });
  }, []);

  return (
    <Router>
      <AppContainer>
        <Header>
          <Link to="/"><h1>{blog.name}</h1></Link>
          <div>{blog.description}</div>
        </Header>
        <Main>
          <Switch>
            <Route path="/p/:pcode">
              <Read />
            </Route>
            <Route path="/">
              <List />
            </Route>
          </Switch>
        </Main>
        <Footer>
          &copy; All react developers, 2013-2020
        </Footer>
      </AppContainer>
    </Router>
  );
}
