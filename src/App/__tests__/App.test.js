import { render, screen } from "@testing-library/react";
import { BlogProvider } from "../../BlogContext";
import App from "../index";

const blogDataMock = {
  "name": "Test Blog",
  "description": "A small test blog",
  "posts": [
    {
      "id": "1201",
      "pcode": "the-first-test-post",
      "title": "Test the first post title",
      "intro": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
      "description": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>"
    },
    {
      "id": "1202",
      "pcode": "second-test-post-title",
      "title": "Test the second post title",
      "intro": "sed do eiusmod tempor incididunt ut labore et dolore ma, sed do eiusmod tempor incididunt ut labore et dolore ma",
      "description": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus.</p><p>Egestas integer eget aliquet nibh praesent tristique magna sit. Amet purus gravida quis blandit turpis cursus in hac habitasse. Elementum eu facilisis sed odio morbi. Eu ultrices vitae auctor eu augue. Elementum tempus egestas sed sed risus pretium. Nunc sed velit dignissim sodales ut. Sem et tortor consequat id porta nibh venenatis. Purus in mollis nunc sed id semper risus in hendrerit. Egestas congue quisque egestas diam in arcu. Dictumst quisque sagittis purus sit. Morbi quis commodo odio aenean sed adipiscing. Fermentum et sollicitudin ac orci phasellus egestas.</p><p>Nisl pretium fusce id velit ut. Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Facilisis mauris sit amet massa. Nulla porttitor massa id neque aliquam vestibulum morbi blandit cursus. Porttitor eget dolor morbi non arcu risus quis varius quam. Malesuada proin libero nunc consequat interdum varius. Volutpat sed cras ornare arcu dui vivamus arcu. Turpis massa sed elementum tempus egestas sed sed. Platea dictumst quisque sagittis purus sit amet. Vitae auctor eu augue ut lectus arcu bibendum at varius. Turpis egestas integer eget aliquet nibh praesent.</p><p>Pretium fusce id velit ut tortor. Lobortis scelerisque fermentum dui faucibus in. Nullam vehicula ipsum a arcu cursus vitae. Tellus rutrum tellus pellentesque eu tincidunt. Est sit amet facilisis magna. Quisque egestas diam in arcu cursus. Commodo viverra maecenas accumsan lacus. Leo urna molestie at elementum eu facilisis sed odio morbi. Enim nec dui nunc mattis enim ut. Mauris pellentesque pulvinar pellentesque habitant morbi tristique senectus.</p><p>In eu mi bibendum neque. Tincidunt dui ut ornare lectus sit amet est placerat. Amet risus nullam eget felis eget. Vitae sapien pellentesque habitant morbi tristique senectus et netus et. Vulputate ut pharetra sit amet. Feugiat sed lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi. Mattis pellentesque id nibh tortor. Laoreet suspendisse interdum consectetur libero. Elit ullamcorper dignissim cras tincidunt lobortis feugiat vivamus. Id diam maecenas ultricies mi eget mauris pharetra et ultrices. Ultricies lacus sed turpis tincidunt. Vitae semper quis lectus nulla at volutpat diam ut. Duis at consectetur lorem donec massa sapien faucibus. Cras ornare arcu dui vivamus arcu felis. Consequat id porta nibh venenatis cras sed felis eget velit.</p>"
    }
  ]
};

jest.mock("../../BlogContext/Services", () => {
  return ({
    fetchBlogData: (cb) => {
      cb(blogDataMock);
    }
  });
});

describe('<App />', () => {

  test("renders the blog title", () => {
    render(
      <BlogProvider><App /></BlogProvider>
    );
    const titleElement = screen.getByText(/Test Blog/ig);
    expect(titleElement).toBeInTheDocument();
  });

  test("renders the blog description", () => {
    render(
      <BlogProvider><App /></BlogProvider>
    );
    const elem = screen.getByText(/A small test blog/ig);
    expect(elem).toBeInTheDocument();
  });

  test("renders the blog list page as default route", () => {
    render(
      <BlogProvider><App /></BlogProvider>
    );
    const elem = screen.getByText(/Blog Posts/ig);
    expect(elem).toBeInTheDocument();
  });

});